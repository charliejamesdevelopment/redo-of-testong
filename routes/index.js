var express = require('express');
var api = require('../utils/api')
var router = express.Router();

router.get('/', function(req, res, next) {
  api.checkAuthentication(req, function(match, user) {
    var object = {
      title: 'FortniteFinder',
      route: "../login",
      user: user
    }
    res.render('templates/index', object);
  })
});

module.exports = router;
